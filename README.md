# DSP Workshop

DSP workshop repository

## Installation

### Ubuntu dependencies

#### Install the following packages:
    sudo apt-get install php7.0 libftdi-dev libusb-1.0-0-dev git git-gui
##
    sudo apt-get install devscripts gcc-i686-linux-gnu libgtk2.0-0:i386 libxtst6:i386 libpangoxft-1.0-0:i386 libidn-dev:i386 libglu1-mesa:i386  libncurses5:i386 libudev1:i386  libusb-1.0:i386 libusb-0.1:i386 gtk2-engines-murrine:i386 libnss3-1d:i386 gtkterm gcc-arm-none-eabi
#### Configure entry
    sudo nano /etc/apt/sources.list
##
Add the following entry to the file and save:

`deb http://cz.archive.ubuntu.com/ubuntu bionic main universer`

Update system:
##
    sudo apt-get update

Install package:
##
    sudo apt-get install libwebkitgtk-1.0-0

### Install OpenOCD

Open terminal in `/home/user`

    mkdir openocd && cd "$_"
##
    wget http://ufpr.dl.sourceforge.net/project/openocd/openocd/0.9.0/openocd-0.9.0.tar.bz2
##
    tar -xvjf openocd-0.9.0.tar.bz2
##
    cd openocd-0.9.0
##
    ./configure --enable-ftdi
##
    make CFLAGS='-Wno-error=implicit-fallthrough'
##
    sudo make install
##
    sudo cp contrib/99-openocd.rules /etc/udev/rules.d/
##
    sudo service udev restart

## Install MCUXpresso

- Link to download: https://www.nxp.com/design/software/development-software/mcuxpresso-software-and-tools-/mcuxpresso-integrated-development-environment-ide:MCUXpresso-IDE

- Install Plugin of MCUXpresso:
    1. Go to `Help -> Eclipse Marketplace...`
    2. Find and install `Eclipse Embedded C/C++`

## Getting started

### Import Project

- Go to `File -> Import... -> C/C++ -> Existing Code as Makefile Project`
- Select the path of the repository

### Select application
- Edit the variable `APP` inside the file `config.mk` to select the target application

### Debug application
- Go to `Debug Configurations`
- Select the profile `GDB OpenOCD Debugging` -> `dsp_workshop`
- Start `Debug`

