/* Copyright 2018, Facundo Larosa - Danilo Zecchin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** @brief This is an application that perfors an addition in C and ASM
 */

/** \addtogroup dsp_workshop
 ** @{ */

/*==================[inclusions]=============================================*/
#include "main.h"

#include "../inc/fusion.h"
#include "board.h"

/*==================[macros and definitions]=================================*/
#define N_SAMPLES 128
/*==================[internal data declaration]==============================*/
uint16_t input1[N_SAMPLES];
uint16_t input2[N_SAMPLES];
uint16_t output_c[N_SAMPLES];
uint16_t output_asm[N_SAMPLES];
uint16_t output_simd[N_SAMPLES];
/*==================[internal functions declaration]=========================*/
/** @brief hardware initialization function
 *	@return none
 */
static void initHardware(void);

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/
static void initHardware(void)
{
    Board_Init();
    SystemCoreClockUpdate();
}

/*==================[external functions definition]==========================*/
int main(void)
{
    volatile uint32_t cycles_c;
    volatile uint32_t cycles_asm;
    volatile uint32_t cycles_simd;
    int i;

    initHardware();

    //Enable profiling counter
    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;

    //Initialize input data and clear output data
    for(i = 0; i < N_SAMPLES; i++)
    {
        input1[i] = i;
        input2[i] = 3 * i;
        output_c[i] = output_asm[i] = output_simd[i] = 0;
    }

    //Clear profiling counter
    DWT->CYCCNT = 0;

    fusion_c(output_c,input1,input2,N_SAMPLES);

    cycles_c = DWT->CYCCNT;

    //Clear profiling counter
    DWT->CYCCNT = 0;

    fusion_asm(output_asm,input1,input2,N_SAMPLES);

    cycles_asm = DWT->CYCCNT;

    DWT->CYCCNT = 0;

    fusion_simd(output_simd,input1,input2,N_SAMPLES);

    cycles_simd = DWT->CYCCNT;

    cycles_c = 0;

    while (1)
    {
        __WFI(); //wfi
    }
}

/** @} doxygen end group definition */

/*==================[end of file]============================================*/
